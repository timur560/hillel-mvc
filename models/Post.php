<?php

class Post
{
    public static function findAll()
    {
        return [
            [
                'title' => 'Test',
                'content' => 'Test content for test blog post',
                'short' => 'Test content',
            ],
            [
                'title' => 'Some another',
                'content' => 'Lorem ipsum dolor sit amet',
                'short' => 'Lorem ipsum',
            ],
        ];
    }

    public static function findOne($id)
    {
        return self::findAll()[$id];
    }
}